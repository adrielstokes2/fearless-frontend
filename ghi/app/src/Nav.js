import { NavLink } from "react-router-dom";

function Nav() {
    return (
      //"to" points to path in App.js
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="container-fluid">
            <NavLink className="navbar-brand" to="">Conference GO! </NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <NavLink className='nav-link active' to='/new'> Home </NavLink>
                </li>
                <li className="nav-item">
                  {/* <a className="nav-link active" aria-current="page" href="new-location.html">New location</a> */}
                  <NavLink className='nav-link' to="/locations/new"> New location </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className='nav-link' to='/conferences/new'> New conferences </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className='nav-link' to='/attendees/new'> Attendees </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className='nav-link' to='/presentations/new'> New presentation </NavLink>
                </li>

              </ul>
            </div>
          </div>
        </nav>
    );
  }

  export default Nav;
